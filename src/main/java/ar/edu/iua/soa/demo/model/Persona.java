package ar.edu.iua.soa.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "persona")
public class Persona {
// id_persona, legajo, cbu,monto_mensual

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_persona")
    private Integer id_persona;

    @OneToMany(mappedBy = "persona")
    @JsonIgnore
    private List<Transaccion> transacciones;

    @Column(name = "legajo")
    private String legajo;

    @Column(name = "cbu")
    private String cbu;

    @Column(name = "monto_mensual")
    private String monto_mensual;

    public Persona(Integer id_persona, String legajo, String cbu, String monto_mensual) {
        super();
        this.id_persona = id_persona;
        this.legajo = legajo;
        this.cbu = cbu;
        this.monto_mensual = monto_mensual;
    }

    public Persona(){
        super();
    }

    public Integer getId_persona() {
        return id_persona;
    }

    public void setId_persona(Integer id_persona) {
        this.id_persona = id_persona;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public String getMonto_mensual() {
        return monto_mensual;
    }

    public void setMonto_mensual(String monto_mensual) {
        this.monto_mensual = monto_mensual;
    }
}
