package ar.edu.iua.soa.demo.business.implementation;

import ar.edu.iua.soa.demo.business.ITransaccionBusiness;
import ar.edu.iua.soa.demo.dto.PostPagoDTO;
import ar.edu.iua.soa.demo.dto.TransaccionDTO;
import ar.edu.iua.soa.demo.model.Transaccion;
import ar.edu.iua.soa.demo.repository.TransaccionRepository;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

@Service
public class TransaccionBusiness implements ITransaccionBusiness {

    @Autowired
    private TransaccionRepository transaccionDAO;

    @Override
    public Transaccion add(Transaccion transaccion){
        String url = "https://iua-service.herokuapp.com/transferir";

        HttpClient client = HttpClientBuilder.create().build();
        // HttpGet request = new HttpGet(url);
        HttpPost postRequest = new HttpPost(url);
        PostPagoDTO postPagoDTO = new PostPagoDTO();
        postPagoDTO.setMonto("1");
        postPagoDTO.setCbu("2");
        Gson gjson = new Gson();
        TransaccionDTO txDto = new TransaccionDTO();
        try {
        StringEntity paramss =new StringEntity(gjson.toJson(postPagoDTO));
            postRequest.setHeader("Content-type", "application/json");
            postRequest.setEntity(paramss);
            HttpResponse response = client.execute(postRequest);
            System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                Gson g = new Gson();
                txDto = g.fromJson(line, TransaccionDTO.class);
            }
            System.out.println(txDto.getCodigo());
            transaccion.setCodigo_aprobacion(txDto.getCodigo());
            transaccion.setEstado_transaccion(txDto.getEstado());
        } catch (Exception e) {
            System.out.println(e);
        }


        System.out.println();
        Date date = new Date();
        transaccion.setFecha_pago(date);
        Transaccion tx = (Transaccion) transaccionDAO.save(transaccion);
        return tx;
    }
}
