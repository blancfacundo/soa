package ar.edu.iua.soa.demo.repository;

import ar.edu.iua.soa.demo.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {
    public Persona findPersonaByLegajo(String legajo);
    public Persona findPersonaByLegajoContaining(String legajo);
    Persona getByLegajo(String legajo);
    // Persona findByLegajo(String legajo);
    // String findByLegajo(String legajo);
    List<Persona> findByLegajo(String legajo);
}
