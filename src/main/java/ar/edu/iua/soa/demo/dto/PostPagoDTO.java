package ar.edu.iua.soa.demo.dto;

public class PostPagoDTO {
    String cbu;
    String monto;

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
}
