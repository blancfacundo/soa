package ar.edu.iua.soa.demo.business;

import ar.edu.iua.soa.demo.model.Transaccion;

public interface ITransaccionBusiness {
    public Transaccion add (Transaccion transaccion);
}
