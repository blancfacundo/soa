package ar.edu.iua.soa.demo.services;

import ar.edu.iua.soa.demo.business.ITransaccionBusiness;
import ar.edu.iua.soa.demo.model.Transaccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constantes.URL_TRANSACCION)
public class TransaccionRESTController {

    @Autowired
    private ITransaccionBusiness transaccionBusiness;

    @RequestMapping(value = { "", "/" }, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Transaccion> addTransaccion(@RequestBody Transaccion transaccion){
        Transaccion tx = transaccionBusiness.add(transaccion);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location", "/transaccion/" + transaccion.getId_transaccion());
        return new ResponseEntity<Transaccion>(tx, responseHeaders, HttpStatus.CREATED);
    }
}
