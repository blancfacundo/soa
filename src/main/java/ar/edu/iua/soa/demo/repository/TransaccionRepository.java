package ar.edu.iua.soa.demo.repository;

import ar.edu.iua.soa.demo.model.Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransaccionRepository extends JpaRepository<Transaccion, Integer> {
}
