package ar.edu.iua.soa.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import javax.sql.DataSource;

@SpringBootApplication(exclude= {SecurityAutoConfiguration.class})
public class DemoApplication implements CommandLineRunner{

    final static Logger log = Logger.getLogger("DemoApplication.class");

	@Autowired
	private DataSource dataSource;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.debug("Datasource actual = " + dataSource);

        /* String url = "https://iua-service.herokuapp.com/producto/1";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        ProductoDTO p = new ProductoDTO();
        try {
            HttpResponse response = client.execute(request);

            System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = "";

            while ((line = rd.readLine()) != null) {
                Gson g = new Gson();
                p = g.fromJson(line, ProductoDTO.class);
                // Object aaa = g.fromJson(line, Object.class)
            }
            System.out.println(p.getNombre());

        } catch (Exception e) {
            System.out.println(e);
        }*/
        		/*RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://iua-service.herokuapp.com/producto/1", String.class);
		System.out.println(resultado);

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(resultado);
        System.out.println(json);

        try {

            URL url = new URL("https://iua-service.herokuapp.com/producto/1");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
*/

    }
}
