package ar.edu.iua.soa.demo.business;

import ar.edu.iua.soa.demo.model.Persona;

public interface IPersonaBusiness {
    public Persona addGet (String legajo);
}
