package ar.edu.iua.soa.demo.services;

import ar.edu.iua.soa.demo.business.IPersonaBusiness;
import ar.edu.iua.soa.demo.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constantes.URL_PERSONA)
public class PersonaRESTController {
    @Autowired
    private IPersonaBusiness iPersonaBusiness;

    @RequestMapping(value = { "", "/" }, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Persona> addTransaccion(@RequestBody String legajo){
        Persona per = iPersonaBusiness.addGet(legajo);
        HttpHeaders responseHeaders = new HttpHeaders();
        // responseHeaders.set("location", "/persona/" + per.getId_persona());
        return new ResponseEntity<Persona>(per, responseHeaders, HttpStatus.CREATED);
    }
}
