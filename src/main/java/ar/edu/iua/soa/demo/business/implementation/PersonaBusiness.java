package ar.edu.iua.soa.demo.business.implementation;

import ar.edu.iua.soa.demo.business.IPersonaBusiness;
import ar.edu.iua.soa.demo.model.Persona;
import ar.edu.iua.soa.demo.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaBusiness implements IPersonaBusiness {

    @Autowired
    private PersonaRepository personaDAO;

    @Override
    public Persona addGet(String legajo){
        System.out.println(personaDAO.findPersonaByLegajo(legajo));
        System.out.println(personaDAO.findPersonaByLegajoContaining(legajo));
        System.out.println(personaDAO.getByLegajo(legajo));
        System.out.println(personaDAO.findByLegajo("1"));
        System.out.println(personaDAO.findByLegajo(legajo));
        System.out.println(personaDAO.findAll());
        return personaDAO.findPersonaByLegajo(legajo);
    }
}
