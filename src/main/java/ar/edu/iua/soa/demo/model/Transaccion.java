package ar.edu.iua.soa.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaccion")
public class Transaccion {
    //id_transaccion, legajo, fecha_pago, monto, estado_transaccion, codigo_aprobacion

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transaccion")
    private Integer id_transaccion;

    @ManyToOne
    @JoinColumn(name = "legajo")
    private Persona persona;

   //Column(name = "legajo")
   //private String legajo;

    @Column(name = "fecha_pago")
    private Date fecha_pago;

    @Column(name = "monto")
    private String monto;

    @Column(name = "estado_transaccion")
    private String estado_transaccion;

    @Column(name = "codigo_aprobacion")
    private String codigo_aprobacion;

    public Transaccion(Integer id_transaccion,Persona persona, Date fecha_pago, String monto, String estado_transaccion, String codigo_aprobacion) {
        super();
        this.id_transaccion = id_transaccion;
        this.persona = persona;
        this.fecha_pago = fecha_pago;
        this.monto = monto;
        this.estado_transaccion = estado_transaccion;
        this.codigo_aprobacion = codigo_aprobacion;
    }

    public Transaccion(){
        super();
    }

    public Integer getId_transaccion() {
        return id_transaccion;
    }

    public void setId_transaccion(Integer id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(Date fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getEstado_transaccion() {
        return estado_transaccion;
    }

    public void setEstado_transaccion(String estado_transaccion) {
        this.estado_transaccion = estado_transaccion;
    }

    public String getCodigo_aprobacion() {
        return codigo_aprobacion;
    }

    public void setCodigo_aprobacion(String codigo_aprobacion) {
        this.codigo_aprobacion = codigo_aprobacion;
    }
}
